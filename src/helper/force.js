import timer from './timer'

function has(object, key) {
  return object != null && Object.prototype.hasOwnProperty.call(object, key)
}

function isFunction(fn) {
  return Object.prototype.toString.call(fn) === '[object Function]'
}

function calcSpring(radius, distanceMax, distanceMin, k, nodes, node) {
  let vx = 0
  let vy = 0
  if (node.edges.length === 0 || nodes.length === 0) {
    return { vx, vy }
  }
  let j = 0
  while (j < node.edges.length) {
    const dst = nodes[node.edges[j]]
    if (node.id !== dst.id) {
      let x = dst.x - node.x
      let y = dst.y - node.y
      let length = Math.sqrt(x * x + y * y)
      if (length === 0) {
        x = Math.random() * (Math.random() > 0.5 ? 1 : -1)
        y = Math.sqrt(1 - x * x) * (Math.random() > 0.5 ? 1 : -1)
        length = radius
      }
      let d = length
      if (length > distanceMax) { // 大于最大长度受到收缩力
        d = distanceMax
      } else if (length < distanceMin) {  // 小于最小长度受到张力
        d = distanceMin
      }
      const f = k * (length - d)
      if (node.edges.length > 30) {
        // console.log(node.edges.length, f, length, distanceMin, distanceMax)
      }
      vx += f * x / length
      vy += f * y / length
    }
    j += 1
  }
  node.x += vx
  node.y += vy
  return { vx, vy }
}

function calcRepulsion(radius, baseCharge, unitCharge, k, nodes, node) {
  let vx = 0
  let vy = 0
  if (nodes.length < 2) {
    return { vx, vy }
  }
  const srcCharge = node.edges.length * unitCharge + baseCharge
  let j = 0
  while (j < nodes.length) {
    const dst = nodes[j]
    if (node.id !== dst.id) {
      const dstCharge = dst.edges.length * unitCharge + baseCharge
      let x = node.x - dst.x
      let y = node.y - dst.y
      let r2 = x * x + y * y
      // 节点重叠，生成随机方向的向量
      if (r2 === 0) {
        x = Math.random() * (Math.random() > 0.5 ? 1 : -1)
        y = Math.sqrt(1 - x * x) * (Math.random() > 0.5 ? 1 : -1)
        r2 = radius * radius
      }
      /*
       * 电荷斥力方程 F = k * Q1 * Q2 / (r * r)
       * 定义：节点的电荷量与节点的边成正比
       */
      const f = k * srcCharge * dstCharge / r2
      const r = Math.sqrt(r2)
      node.x += f * x / r
      node.y += f * y / r
      vx += f * x / r
      vy += f * y / r
    }
    j += 1
  }
  return { vx, vy }
}

function calcGravity(radius, k, center, nodes, sx, sy) {
  let maxOffset = 0
  const nx = center.x - sx / nodes.length
  const ny = center.y - sy / nodes.length
  let i = 0
  while (i < nodes.length) {
    const node = nodes[i]
    i += 1
    let x = center.x - node.x - nx
    let y = center.y - node.y - ny
    let length = Math.sqrt(x * x + y * y)
    let d = length
    if (length > radius) { // 大于最大长度受到收缩力
      d = radius
    }
    /*
     * 向心力可以看成是节点受到一个中心点牵引的弹力，
     * 因此使用弹力方程 F = kx 可以计算得到此节点的受到的弹力大小
     */
    const f = k * (length - d)
    // 力作用到节点，修改节点位移向量坐标
    const vx = f * x / length + nx
    const vy = f * y / length + ny
    node.x += vx
    node.y += vy
    const offset = Math.sqrt(vx * vx + vy * vy)
    if (offset > maxOffset) {
      maxOffset = offset
    }
  }
  return maxOffset
}

export default function() {

  let iterations = 100  // 迭代次数
  let distanceMin = 30   // 两点间产生弹力的最小距离，小于这个距离，产生斥力
  let distanceMax = 100  // 两点间产生弹力的最da距离，大于这个距离，产生引力
  let springK = 1.87  // 引力常数，引力t = k * (d - L)， d是两点实际距离
  let chargeK = 0.0004  // 斥力常数
  let baseCharge = 4000 // 基本的电荷量
  let unitCharge = 50  // 电荷量增量
  let stableOffset = 2  // 最小力，当所有力小于最小力时停止迭代
  let center = null
  let radius = 300    // 产生向心力的半径
  let centerK = 0.01  // 向心弹力常数
  let jiggleRadius = 20

  let simulation
  let stepper
  let nodes = []
  let edges = []
  let maxEdgeCount = 0
  let handleTickEvent = () => {}
  let handleEndEvent = () => {}

  const force = function() {
    const hasCenter = center && has(center, 'x') && has(center, 'y')
    let maxOffset = 0
    // 遍历所有节点，依次做出位置移动
    let sx = 0
    let sy = 0
    let i = 0
    while (i < nodes.length) {
      const node = nodes[i]
      i += 1
      // 叠加力
      let vx = 0
      let vy = 0
      let rst
      // 收集边的引力
      const k = springK / maxEdgeCount
      rst = calcSpring(jiggleRadius, distanceMax, distanceMin, k, nodes, node)
      vx += rst.vx
      vy += rst.vy
      // 收集节点间的斥力
      rst = calcRepulsion(jiggleRadius, baseCharge, unitCharge, chargeK, nodes, node)
      vx += rst.vx
      vy += rst.vy
      // 记录最大偏移
      const offset = Math.sqrt(vx * vx + vy * vy)
      if (offset > maxOffset) {
        maxOffset = offset
      }
      sx += node.x
      sy += node.y
    }
    // 计算中心引力
    if (hasCenter) {
      const offset = calcGravity(radius, centerK, center, nodes, sx, sy)
      if (offset > maxOffset) {
        maxOffset = offset
      }
    }
    return maxOffset
  }

  const tick = function() {
    iterations -= 1
    if (iterations < 0) {
      stepper.stop()
      handleEndEvent(simulation)
    }
    const maxOffset = force()
    // 如果所有节点的最大偏移小于最小偏移量，认为系统已经稳定，停止迭代
    if (maxOffset < stableOffset) {
      stepper.stop()
      handleEndEvent(simulation)
    }
    let i = 0
    while (i < nodes.length) {
      const node = nodes[i]
      i += 1
      if (typeof node.fx === 'number') {
        node.x = node.fx
      }
      if (typeof node.fy === 'number') {
        node.y = node.fy
      }
    }
    handleTickEvent(simulation)
  }

  const initialize = function() {
    if (!nodes || !Array.isArray(nodes)) {
      return
    }
    let data = {}
    const centerX = center && center.x || 0
    const centerY = center && center.y || 0
    // 遍历节点，生成图的节点索引
    let i = 0
    while (i < nodes.length) {
      const node = nodes[i]
      if (has(data, node.id)) {
        throw new Error('duplicate node: ' + node.id)
      }
      data[node.id] = i
      node.edges = []
      node.index = i
      i += 1
      if (isNaN(node.x) || isNaN(node.y)) {
        const initialRadius = 10
        const initialAngle = Math.PI * (3 - Math.sqrt(5))
        const radius = initialRadius * Math.sqrt(i)
        const angle = i * initialAngle
        node.x = radius * Math.cos(angle) + centerX
        node.y = radius * Math.sin(angle) + centerY
      }
      if (isNaN(node.vx) || isNaN(node.vy)) {
        node.vx = 0
        node.vy = 0
      }
    }
    // 如果有边集，遍历边集，生成图索引
    i = 0
    if (edges && Array.isArray(edges)) {
      while (i < edges.length) {
        const edge = edges[i]
        i += 1
        if (!has(data, edge.source)) {
          throw new Error('missing node: ' + edge.source)
        }
        if (!has(data, edge.target)) {
          throw new Error('missing node: ' + edge.target)
        }
        const srcIndex = data[edge.source]
        const dstIndex = data[edge.target]
        if (nodes[srcIndex].edges.indexOf(dstIndex) === -1) {
          nodes[srcIndex].edges.push(dstIndex)
          if (nodes[srcIndex].edges.length > maxEdgeCount) {
            maxEdgeCount = nodes[srcIndex].edges.length
          }
        }
        if (nodes[dstIndex].edges.indexOf(srcIndex) === -1) {
          nodes[dstIndex].edges.push(srcIndex)
          if (nodes[dstIndex].edges.length > maxEdgeCount) {
            maxEdgeCount = nodes[dstIndex].edges.length
          }
        }
      }
    }
    data = null
  }

  stepper = timer(tick)

  simulation = {
    tick: tick,
    restart: function() {
      stepper.restart(tick)
      return simulation
    },
    stop: function() {
      stepper.stop()
      return simulation
    },
    data: function(arg) {
      if (arg === undefined) {
        return { nodes, edges }
      }
      if (has(arg, 'nodes')) {
        if (Array.isArray(arg.nodes)) {
          nodes = arg.nodes
        } else {
          throw new Error('nodes type error: require Array')
        }
      }
      if (has(arg, 'edges')) {
        if (Array.isArray(arg.edges)) {
          edges = arg.edges
        } else {
          throw new Error('edges type error: require Array')
        }
      }
      initialize()
      return simulation
    },
    on: function(name, arg) {
      if (arg === undefined || arg === null) {
        return simulation
      }
      if (name === 'tick' && isFunction(arg)) {
        handleTickEvent = arg
      }
      if (name === 'end' && isFunction(arg)) {
        handleEndEvent = arg
      }
      return simulation
    },
    iterations: function(arg) {
      if (arg === undefined) {
        return iterations
      }
      if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
        iterations = arg
      }
      return simulation
    },
    // 停止迭代的最小位移
    stableOffset: function(arg) {
      if (arg === undefined) {
        return stableOffset
      }
      if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
        stableOffset = arg
      }
      return simulation
    },
    force: {
      // 产生斥力的弹性形变长度
      distanceMin: function(arg) {
        if (arg === undefined) {
          return distanceMin
        }
        if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
          distanceMin = arg
        }
        return simulation
      },
      // 产生引力的弹性形变长度
      distanceMax: function(arg) {
        if (arg === undefined) {
          return distanceMax
        }
        if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
          distanceMax = arg
        }
        return simulation
      },
      // 弹性形变常量
      springK: function(arg) {
        if (arg === undefined) {
          return springK
        }
        if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
          springK = arg
        }
        return simulation
      },
      // 电荷斥力常量
      chargeK: function(arg) {
        if (arg === undefined) {
          return chargeK
        }
        if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
          chargeK = arg
        }
        return simulation
      },
      // 电荷的基本电荷量
      baseCharge: function(arg) {
        if (arg === undefined) {
          return baseCharge
        }
        if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
          baseCharge = arg
        }
        return simulation
      },
      // 电荷增量的单位电荷量，随着连接数的增加，电荷的电荷量增加
      unitCharge: function(arg) {
        if (arg === undefined) {
          return unitCharge
        }
        if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
          unitCharge = arg
        }
        return simulation
      },
      // 向心弹力常量
      centerK: function(arg) {
        if (arg === undefined) {
          return centerK
        }
        if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
          centerK = arg
        }
        return simulation
      },
      // 向心引力的中心点坐标
      center: function(arg) {
        if (arg === undefined) {
          return center
        } else if (arg === null) {
          center = null
          return simulation
        }
        const x = arg.x
        const y = arg.y
        if (typeof x !== 'number' || isNaN(x) || Math.abs(x) === Infinity) {
          center = null
          return simulation
        }
        if (typeof y !== 'number' || isNaN(y) || Math.abs(y) === Infinity) {
          center = null
          return simulation
        }
        center = { x, y }
        return simulation
      },
      // 向心引力半径
      radius: function(arg) {
        if (arg === undefined) {
          return radius
        }
        if (typeof arg === 'number' && arg >= 0 && Math.abs(arg) !== Infinity) {
          radius = arg
        }
        return simulation
      },
      // 节点发生重叠时的的力大小
      jiggleRadius: function(arg) {
        if (arg === undefined) {
          return jiggleRadius
        }
        if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
          jiggleRadius = arg
        }
        return simulation
      }
    }
  }
  return simulation
}
