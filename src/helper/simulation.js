import _ from 'lodash'
import {
  merge,
} from './util'
import force from './force.js'

const dftOptions = {
  enable: true,
  iterations: 500,    // 迭代次数
  stableOffset: 1,    // 最小力，当所有力小于最小力时停止迭代
  force: {
    distanceMin: null,  // 两点间产生弹力的最小距离，小于这个距离，产生斥力
    distanceMax: null,  // 两点间产生弹力的最da距离，大于这个距离，产生引力
    springK: null,    // 引力常数，引力t: k * (d - L)， d是两点实际距离
    chargeK: null,  // 斥力常数
    baseCharge: null,     // 基本的电荷量
    unitCharge: null,   // 电荷量增量
    radius: null,      // 产生向心力的半径
    centerK: null,    // 向心弹力常数
    jiggleRadius: null,
    center: {
      x: 0,
      y: 0,
    }
  },
  speed: {
    alpha: null,            // alpha 初值，仿真速度
    alphaMin: null,       // alpha 最小值
    alphaTarget: null,   // alpha 目标值
    alphaDecay: null,    // alpha 衰减系数
    velocityDecay: null,  // 速度衰减系数，摩擦力
    onTick: null,
    onEnd: null,
  },
  d3force: {
  }
}

const dftUpdateView = function(updateNodes, updateEdges) {
  return type => {
    updateNodes(type)
    updateEdges(type)
  }
}

const dftOnTick = function(updateView) {
  return () => updateView('tick')
}

const dftOnEnd = function(updateView) {
  return () => updateView('end')
}

export default class Simulation {

  _opts = null
  _eventer = null
  _updateNodes = null
  _updateEdges = null
  _$graph = null
  _simulator = null
  _force = null

  constructor(options) {
    if (_.has(options, 'simulation')) {
      this._opts = merge({}, dftOptions, options.simulation)
      options.simulation = this._opts
    } else {
      this._opts = merge({}, dftOptions, options)
    }
    this._simulator = force()
  }

  create($graph, eventer, update) {
    if (update && !_.isFunction(update.nodes)) {
      throw new Error('updateNodes must be function')
    }
    if (update && !_.isFunction(update.edges)) {
      throw new Error('updateEdges must be function')
    }
    this._eventer = eventer
    this._$graph = $graph
    this._updateNodes = update.nodes
    this._updateEdges = update.edges
    this._eventer.emit('simulation.create')
    return this
  }

  update(nodes, edges) {
    this._eventer.emit('simulation.start')
    if (this._opts.enable && !(_.isEmpty(nodes) && _.isEmpty(edges))) {
      // 更新前清除位置信息，重新生成
      // 使用旧的位置信息，会导致图形慢慢向外延展，而不会收敛
      _.forEach(nodes, node => {
        delete node.x
        delete node.y
        delete node.vx
        delete node.vy
      })
      this.stop()
      this._setParams()
      this._simulator.data({ nodes, edges })
      this.restart()
    } else {
      this.stop()
      this._eventer.emit('simulation.end')
    }
  }

  restart() {
    this._eventer.emit('simulation.restart')
    this._simulator.restart()
  }

  stop() {
    this._simulator.stop()
    this._eventer.emit('simulation.stop')
  }

  tick() {
    this._simulator.tick()
  }

  destroy() {
    this._simulator = null
    this._updateNodes = null
    this._updateEdges = null
    this._opts = null
    this._eventer.emit('simulation.destroy')
    this._eventer = null
  }

  _setParams() {
    if (this._opts.enable) {
      const rect = this._$graph.node().getBoundingClientRect()
      this._opts.force.center = {
        x: rect.width / 2,
        y: rect.height / 2,
      }
      this._simulator.iterations(this._opts.iterations)
      _.forEach(this._opts.force, (arg, param) => {
        this._simulator.force[param](arg)
      })
      // filter 会影响 simulation 效率，所以开始simulation前去掉所有的filter
      // 结束后再添加回来
      const $filters = this._$graph.selectAll('filter').remove()
      const updateView = dftUpdateView(this._updateNodes, this._updateEdges)
      const onTick = dftOnTick(updateView)
      const onEnd = dftOnEnd(updateView)
      this._simulator.on('tick', () => {
        if (_.isFunction(this._opts.onTick)) {
          this._opts.onTick(onTick)
        } else {
          onTick()
        }
        this._eventer.emit('simulation.tick')
      }).on('end', () => {
        if (_.isFunction(this._opts.onEnd)) {
          this._opts.onEnd(onEnd)
        } else {
          onEnd()
        }
        // 将删除的filter添加回DOM
        $filters.nodes().forEach(node => {
          this._$graph.select('defs').append(() => node)
        })
        this._eventer.emit('simulation.end')
      })
    }
  }
}
