function has(object, key) {
  return object != null && Object.prototype.hasOwnProperty.call(object, key)
}

export default function() {
  let nodes = []
  let edges = []
  let charges = []
  let baseCharge = 4000
  let unitCharge = 10
  let kappa = 0.0004
  const jiggleRadius = 10

  function force(arg) {
    if (nodes.length < 2) {
      return
    }
    let i = 0
    while (i < nodes.length) {
      const src = nodes[i]
      const srcCharge = charges[i].length * unitCharge + baseCharge
      // 收集节点间的斥力
      let vx = 0
      let vy = 0
      let j = 0
      while (j < nodes.length) {
        if (i !== j) {
          const dst = nodes[j]
          const dstCharge = charges[j].length * unitCharge + baseCharge
          let x = src.x - dst.x
          let y = src.y - dst.y
          let r2 = x * x + y * y
          // 节点重叠，生成随机方向的向量
          if (r2 === 0) {
            x = Math.random() * (Math.random() > 0.5 ? 1 : -1)
            y = Math.sqrt(1 - x * x) * (Math.random() > 0.5 ? 1 : -1)
            r2 = jiggleRadius * jiggleRadius
          }
          /*
           * 电荷斥力方程 F = k * Q1 * Q2 / (r * r)
           * 定义：节点的电荷量与节点的边成正比
           */
          const f = kappa * srcCharge * dstCharge / r2
          const r = Math.sqrt(r2)
          vx += f * x / r
          vy += f * y / r
        }
        j += 1
      }
      // 力作用到节点，修改节点位移向量坐标
      src.x += vx
      src.y += vy
      i += 1
    }
  }

  function initialize() {
    if (!nodes || !Array.isArray(nodes)) {
      return
    }
    if (nodes.length === 0) {
      return
    }
    let data = {}
    // 遍历节点，生成图的节点索引
    let i = 0
    while (i < nodes.length) {
      const node = nodes[i]
      if (has(data, node.id)) {
        throw new Error('duplicate node: ' + node.id)
      }
      data[node.id] = i
      charges[i] = []
      i += 1
    }
    // 如果有边集，遍历边集，生成图索引
    if (edges && Array.isArray(edges)) {
      i = 0
      while (i < edges.length) {
        const edge = edges[i]
        i += 1
        if (!has(data, edge.source)) {
          throw new Error('missing node: ' + edge.source)
        }
        if (!has(data, edge.target)) {
          throw new Error('missing node: ' + edge.target)
        }
        const sIndex = data[edge.source]
        const tIndex = data[edge.target]
        if (charges[sIndex].indexOf(tIndex) === -1) {
          charges[sIndex].push(tIndex)
        }
        if (charges[tIndex].indexOf(sIndex) === -1) {
          charges[tIndex].push(sIndex)
        }
      }
    }
    data = null
  }

  force.initialize = function(arg) {
    nodes = arg
    initialize()
  }

  force.edges = function(arg) {
    if (arg === undefined) {
      return arg
    }
    if (Array.isArray(arg)) {
      edges = arg
    } else {
      throw new Error('edges type error: require Array')
    }
    initialize()
    return force
  }

  force.kappa = function(arg) {
    if (arg === undefined) {
      return kappa
    }
    if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
      kappa = arg
    }
    return force
  }
  // 电荷的基本电荷量
  force.baseCharge = function(arg) {
    if (arg === undefined) {
      return baseCharge
    }
    if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
      baseCharge = arg
    }
    return force
  }
  // 电荷增量的单位电荷量，随着连接数的增加，电荷的电荷量增加
  force.unitCharge = function(arg) {
    if (arg === undefined) {
      return unitCharge
    }
    if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
      unitCharge = arg
    }
    return force
  }

  return force
}
