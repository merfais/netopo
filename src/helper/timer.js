const frameTimeout = 17

function setFrame(frame) {
  const raf = typeof window === 'object' && window.requestAnimationFrame ?
    window.requestAnimationFrame.bind(window) :
    cb => setTimeout(cb, frameTimeout)
  raf(frame)
}

function Timer(tick) {
  if (tick === null || tick === undefined || typeof tick !== 'function') {
    throw new Error('require a function params when new Timer')
  }
  this._tick = tick
  this._running = false
}

Timer.prototype.restart = function() {
  if (typeof this._tick !== 'function') {
    return
  }

  const self = this
  function loop() {
    /* eslint-disable no-underscore-dangle */
    if (self._running) {
      self._tick()
      setFrame(loop)
    }
    /* eslint-enable no-underscore-dangle */
  }

  this._running = true
  loop()
}

Timer.prototype.stop = function() {
  this._running = false
}


export default function(tick) {

  const timer = new Timer(tick)

  return timer
}
