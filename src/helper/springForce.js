function has(object, key) {
  return object != null && Object.prototype.hasOwnProperty.call(object, key)
}

export default function() {
  let nodes = []
  let edges = []
  let springs = []
  let distanceMin = 30   // 两点间产生弹力的最小距离，小于这个距离，产生斥力
  let distanceMax = 60  // 两点间产生弹力的最da距离，大于这个距离，产生引力
  let kappa = 0.2  // 引力常数，引力t = k * (d - L)， d是两点实际距离
  const jiggleRadius = 10

  function force(arg) {
    let i = 0
    while (i < nodes.length) {
      if (springs[i].length === 0) {
        i += 1
        // eslint-disable-next-line no-continue
        continue
      }
      const src = nodes[i]
      /*
       * 为了使系统更快更好的趋于稳定，认定节点的弹性系数与其链接数成反比
       * 连接数使用m表示，基本弹力系数使用K，实际弹力系数使用k表示
       * 结合弹力公式f = kx，则节点受到的弹力可表示为 f = k / m * x
       */
      const srcK = kappa / springs[i].length
      // 收集节点间的斥力
      let vx = 0
      let vy = 0
      let j = 0
      // 收集边的引力
      while (j < springs[i].length) {
        if (i !== j) {
          const dst = nodes[springs[i][j]]
          let x = dst.x - src.x
          let y = dst.y - src.y
          let length = Math.sqrt(x * x + y * y)
          if (length === 0) {
            x = Math.random() * (Math.random() > 0.5 ? 1 : -1)
            y = Math.sqrt(1 - x * x) * (Math.random() > 0.5 ? 1 : -1)
            length = jiggleRadius
          }
          let d = length
          if (length > distanceMax) { // 大于最大长度受到收缩力
            d = distanceMax
          } else if (length < distanceMin) {  // 小于最小长度受到张力
            d = distanceMin
          }
          const f = srcK * (length - d)
          vx += f * x / length
          vy += f * y / length
        }
        j += 1
      }
      // 力作用到节点，修改节点位移向量坐标
      src.x += vx
      src.y += vy
      i += 1
    }
  }

  function initialize() {
    if (!nodes || !Array.isArray(nodes)) {
      return
    }
    if (nodes.length === 0) {
      return
    }
    let data = {}
    // 遍历节点，生成图的节点索引
    let i = 0
    while (i < nodes.length) {
      const node = nodes[i]
      if (has(data, node.id)) {
        throw new Error('duplicate node: ' + node.id)
      }
      data[node.id] = i
      springs[i] = []
      i += 1
    }
    // 如果有边集，遍历边集，生成图索引
    if (edges && Array.isArray(edges)) {
      i = 0
      while (i < edges.length) {
        const edge = edges[i]
        i += 1
        if (!has(data, edge.source)) {
          throw new Error('missing node: ' + edge.source)
        }
        if (!has(data, edge.target)) {
          throw new Error('missing node: ' + edge.target)
        }
        const sIndex = data[edge.source]
        const tIndex = data[edge.target]
        if (springs[sIndex].indexOf(tIndex) === -1) {
          springs[sIndex].push(tIndex)
        }
        if (springs[tIndex].indexOf(sIndex) === -1) {
          springs[tIndex].push(sIndex)
        }
      }
    }
    data = null
  }

  force.initialize = function(arg) {
    nodes = arg
    initialize()
  }

  force.edges = function(arg) {
    if (arg === undefined) {
      return arg
    }
    if (Array.isArray(arg)) {
      edges = arg
    } else {
      throw new Error('edges type error: require Array')
    }
    initialize()
    return force
  }

  force.kappa = function(arg) {
    if (arg === undefined) {
      return kappa
    }
    if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
      kappa = arg
    }
    return force
  }
  // 电荷的基本电荷量
  force.distanceMin = function(arg) {
    if (arg === undefined) {
      return distanceMin
    }
    if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
      distanceMin = arg
    }
    return force
  }
  // 电荷增量的单位电荷量，随着连接数的增加，电荷的电荷量增加
  force.distanceMax = function(arg) {
    if (arg === undefined) {
      return distanceMax
    }
    if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
      distanceMax = arg
    }
    return force
  }

  return force
}
