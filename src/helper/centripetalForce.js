export default function() {
  let nodes = []
  let kappa = 0.01  // 引力常数，引力f = k * (d - L)， d是两点实际距离
  let radius = 200
  let center = null

  function force(arg) {
    if (!center) {
      return
    }
    let i = 0
    while (i < nodes.length) {
      const node = nodes[i]
      let x = center.x - node.x
      let y = center.y - node.y
      let length = Math.sqrt(x * x + y * y)
      let d = length
      if (length > radius) { // 大于最大长度受到收缩力
        d = radius
      }
      const f = kappa * (length - d)
      // 力作用到节点，修改节点位移向量坐标
      node.x += f * x / length
      node.y += f * y / length
      i += 1
    }
  }

  function initialize() {
  }

  force.initialize = function(arg) {
    nodes = arg
    initialize()
  }

  force.kappa = function(arg) {
    if (arg === undefined) {
      return kappa
    }
    if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
      kappa = arg
    }
    return force
  }
  // 电荷的基本电荷量
  force.radius = function(arg) {
    if (arg === undefined) {
      return radius
    }
    if (typeof arg === 'number' && arg >= 0 && arg !== Infinity) {
      radius = arg
    }
    return force
  }
  // 向心引力的中心点坐标
  force.center = function(arg) {
    if (arg === undefined) {
      return center
    } else if (arg === null) {
      center = null
      return force
    }
    const x = arg.x
    const y = arg.y
    if (typeof x !== 'number' || isNaN(x) || Math.abs(x) === Infinity) {
      center = null
      return force
    }
    if (typeof y !== 'number' || isNaN(y) || Math.abs(y) === Infinity) {
      center = null
      return force
    }
    center = { x, y }
    return force
  }

  return force
}
