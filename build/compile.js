'use strict'

const webpack = require('webpack')
const webpackConfig = require('./webpack.compile.conf')
const utils = require('./utils.js')

const tmpPath = utils.resolve('./tmp')

const compileCallback = function(err, multiStats) {
  if (err) {
    console.log(err)
    process.exit(1)
  }
  const printStats = function(stats) {
    if (stats.hasErrors()) {
      console.log(stats.compilation.errors)
      console.log('\n\n')
      process.exit(1)
    }
    if (stats.hasWarnings()) {
      console.log(stats.compilation.warnings)
      console.log('\n\n')
      process.exit(1)
    }
  }
  if ('stats' in multiStats  && Array.isArray(multiStats.stats)) {
    multiStats.stats.forEach(item => {
      printStats(item)
    })
  } else {
    printStats(multiStats)
  }
  utils.exec(`rm -rf ${tmpPath}`)
}

utils.exec(`rm -rf ${tmpPath}`).then(() => {
  return utils.exec(`mkdir -p ${tmpPath}`)
}).then(() => {
  webpack(webpackConfig, compileCallback)
})
