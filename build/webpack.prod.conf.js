const webpack = require('webpack')
const merge = require('webpack-merge')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const webpackConfig = require('./webpack.base.conf')
const utils = require('./utils.js')

const prod = {
  devtool: 'source-map',
  module: {
    rules: [
      utils.genVueLoader(true),
      utils.genCssLoader(true),
    ],
  },
  plugins: [
    // extract css into its own file
    new ExtractTextPlugin({
      filename: '[name].[hash:6].css',
      // Setting the following option to `false` will not extract CSS from codesplit chunks.
      // Their CSS will instead be inserted dynamically with style-loader when the codesplit chunk has been loaded by webpack.
      // It's currently set to `true` because we are seeing that sourcemaps are included in the codesplit bundle as well when it's `false`,
      // increasing file size: https://github.com/vuejs-templates/webpack/issues/1110
      allChunks: true,
    }),
    // Compress extracted CSS. We are using this plugin so that possible
    // duplicated CSS from different components can be deduped.
    new OptimizeCSSPlugin({
      cssProcessorOptions: {
        safe: true,
        map: {
          inline: false
        }
      }
    }),
  ]
}

if (!process.env.npm_config_pretty) {
  prod.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      mangle: true,
      sourceMap: true,
      parallel: true,
    })
  )
}

module.exports = merge(webpackConfig.base, webpackConfig.prod, prod)
