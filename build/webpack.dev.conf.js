const webpack = require('webpack')
const merge = require('webpack-merge')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const webpackConfig = require('./webpack.base.conf.js')
const utils = require('./utils.js')

const extract = !!process.env.npm_config_extract

const plugins = extract ? [
  // extract css into its own file
  new ExtractTextPlugin({
    filename: '[name].[hash:6].css',
    // Setting the following option to `false` will not extract CSS from codesplit chunks.
    // Their CSS will instead be inserted dynamically with style-loader when the codesplit chunk has been loaded by webpack.
    // It's currently set to `true` because we are seeing that sourcemaps are included in the codesplit bundle as well when it's `false`,
    // increasing file size: https://github.com/vuejs-templates/webpack/issues/1110
    allChunks: true,
  }),
  // Compress extracted CSS. We are using this plugin so that possible
  // duplicated CSS from different components can be deduped.
  new OptimizeCSSPlugin({
    cssProcessorOptions: {
      safe: true,
      map: {
        inline: false
      }
    }
  }),
] : []

const dev = {
  module: {
    rules: [
      utils.genVueLoader(extract),
      utils.genCssLoader(extract),
    ],
  },
  plugins,
}

module.exports = merge(webpackConfig.base, webpackConfig.dev, dev)
