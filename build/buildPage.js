'use strict'

const chalk = require('chalk')
const webpack = require('webpack')
const webpackConfig = require('./webpack.prod.conf')
const utils = require('./utils.js')

const distPath = utils.resolve('./pages')

utils.exec(`rm -rf ${distPath}`).then(() => {
  return utils.exec(`mkdir -p ${distPath}`)
}).then(() => {
  webpack(webpackConfig, (err, stats) => {
    if (err) {
      console.log(err)
      process.exit(1)
    }
    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'))
      console.log(stats.compilation.errors)
      console.log('\n\n')
      process.exit(1)
    }
    if (stats.hasWarnings()) {
      console.log(chalk.red('  Build failed with errors.\n'))
      console.log(stats.compilation.warnings)
      console.log('\n\n')
      process.exit(1)
    }

    const statsString = stats.toString({
      colors: true,
      modules: false,
    })
    console.log(statsString)
    console.log(chalk.green.bold('  Build complete.\n'))
  })
})
