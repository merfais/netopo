const webpack = require('webpack')
const merge = require('webpack-merge')
const webpackConfig = require('./webpack.base.conf.js')
const utils = require('./utils.js')

const compile = {
  module: {
    rules: [
      utils.genVueLoader(false),
      utils.genCssLoader(false),
    ],
  },
}

module.exports = merge(webpackConfig.base, webpackConfig.prod, compile)
