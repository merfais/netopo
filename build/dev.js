'use strict'

const chalk = require('chalk')
const express = require('express')
const webpack = require('webpack')
const portfinder = require('portfinder')
const webpackConfig = require('./webpack.dev.conf')
const utils = require('./utils.js')

const findPort = function() {
  return utils.getPort().then(port => {
    port = port || (process.env.PORT && Number(process.env.PORT)) || 8000
    portfinder.basePort = port
    return new Promise((resolve, reject) => {
      portfinder.getPort((err, port) => {
        if (err) {
          reject(err)
        } else {
          process.env.PORT = port
          resolve(port)
        }
      })
    })
  })
}

// remove the old assets code
findPort().then(port => {
  const host = process.env.HOST || '0.0.0.0'
  const uri = `http://:${host}:${port}`

  const reporter = function(middlewareOptions, options) {
    const { log, state, stats } = options;
    if (state) {
      const statsInfo = stats.toString({
        chunks: false,
        colors: true,
        modules: false,
      })
      console.log(
        chalk.blue.bold('*********************  compile summary  ***********************\n')
        + statsInfo
        + '\n\ncompiled at ' + chalk.yellow(new Date())
        + chalk.blue.bold('\n***************************************************************\n')
      )
      if (stats.hasErrors()) {
        console.log(chalk.red.bold(`\n!!!!!!!!!!!!!! HAS  ERROR  !!!!!!!!!!!!!!!!\n`))
      } else if (stats.hasWarnings()) {
        console.log(chalk.yellow.bold(`\n!!!!!!!!!!!!!! HAS  WARNING  !!!!!!!!!!!!!!!!\n`))
      } else {
      }
      console.log(chalk.green.bold('>> Listening at ' + uri + '\n'))
    }
  }

  const compiler = webpack(webpackConfig)
  compiler.run(function(err, stats) {
    if (err) {
      throw  err
    }
  })

  const devMiddleware = require('webpack-dev-middleware')(compiler, {
    logLevel: 'silent',
    reporter,
  })
  const hotMiddleware = require('webpack-hot-middleware')(compiler, {
    log: false,
  })

  // force page reload when html-webpack-plugin template changes
  compiler.plugin('compilation', function (compilation) {
    compilation.plugin('html-webpack-plugin-after-emit', function (data, cb) {
      hotMiddleware.publish({ action: 'reload' })
      cb()
    })
  })

  const app = express()
  app.use(devMiddleware)
  app.use(hotMiddleware)
  // handle fallback for HTML5 history API
  app.use(require('connect-history-api-fallback')())
  // serve pure static assets
  app.use(express.static(webpackConfig.output.path))
  return app.listen(port, host, err => {
    if (err) {
      throw err
    }
  })
}).catch(err => {
  console.log(err)
  console.log(chalk.red.bold(`\n!!!!!!!!!!!!!! HAS  ERROR  !!!!!!!!!!!!!!!!\n\n`))
})
