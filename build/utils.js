const path = require('path')
const fs = require('fs')
const chalk = require('chalk')
const execProcess = require('child_process').exec

function resolve(dir) {
  return path.join(__dirname, '../', dir)
}

// 读取.devrc文件中的 dev_server_port 字段作为服务器的启动端口
function getPort() {
  const pathResolve = resolve
  return new Promise((resolve, reject) => {
    fs.readFile(pathResolve('.devrc'), 'utf8', (err, data) => {
      let port = 0
      if (err) {
        console.log(chalk.yellow.bold(err.message + '\n'))
        resolve(port)
      } else {
        const contentList = data.split('\n')
        contentList.forEach(item => {
          let line = item.split('=')
          if (line[0] === 'dev_server_port') {
            port = parseInt(line[1])
          }
        })
        resolve(port)
      }
    })
  })
}

function exec(str, options ={}) {
  return new Promise((resolve, reject) => {
    execProcess(str, options, (err, stdout, stderr) => {
      if (err) {
        console.log(chalk.red.bold('\n' + err + '\n'))
        reject(err)
      }
      resolve(stdout, stderr)
    })
  })
}

const baseLoaders = [{
  loader: 'css-loader',
  options: {
    import: true,
    importLoaders: true,
  }
}, {
  loader: 'postcss-loader',
  options: {
  }
}]

function generateLoaders(name, options = {}, extract = false) {
  if (typeof name === 'boolean') {
    extract = name
    name = ''
    options = {}
  }
  if (typeof options === 'boolean') {
    extract = options
    options = {}
  }
  let loaders = baseLoaders
  if (name) {
    loaders = baseLoaders.concat([{
      loader: name + '-loader',
      options,
    }])
  }
  if (extract) {
    const ExtractTextPlugin = require('extract-text-webpack-plugin')
    return ExtractTextPlugin.extract({
      use: loaders,
      fallback: 'vue-style-loader'
    })
  } else {
    return [{
      loader: 'vue-style-loader',
      options: {}
    }].concat(loaders)
  }
}

const eslintLoader =  {
  test: /\.(js|vue)$/,
  exclude: /node_modules/,
  include: resolve('src'),
  enforce: 'pre',
  use: [{
    loader: 'eslint-loader',
    options: {
      formatter: require('eslint-formatter-linux'),
    }
  }]
}

function genVueLoader(extract) {
  return {
    test: /\.vue$/,
    exclude: /node_modules/,
    include: [
      resolve('src'),
      resolve('demo'),
    ],
    use: [
      {
        loader: 'vue-loader',
        options: {
          loaders: {
            css: generateLoaders(extract),
          },
          transformToRequire: {
            video: ['src', 'poster'],
            source: 'src',
            img: 'src',
            image: 'xlink:href'
          }
        }
      }
    ]
  }
}

function genCssLoader(extract) {
  return {
    test: /\.css$/,
    include: [
      resolve('src'),
      resolve('demo'),
    ],
    use: generateLoaders(extract),
  }
}

function genLessLoader(extract) {
  return {
    test: /\.less$/,
    include: [
      resolve('src'),
      resolve('demo'),
    ],
    use: generateLoaders('less', extract),
  }
}

function printStats(stats) {
  if (stats.hasErrors()) {
    console.log(stats.compilation.errors)
    console.log('\n\n')
    process.exit(1)
  }
  if (stats.hasWarnings()) {
    console.log(stats.compilation.warnings)
    console.log('\n\n')
    process.exit(1)
  }
}

function genCompileCallback(callback) {
  return (err, multiStats) => {
    if (err) {
      console.log(err.stack || err)
      if (err.details) {
        console.log(err.details)
      }
      console.log('\n\n')
      process.exit(1)
    }
    if (Array.isArray(multiStats)) {
      multiStats.forEach(item => {
        printStats(item)
      })
    } else if ('stats' in multiStats && Array.isArray(multiStats.stats)) {
      multiStats.stats.forEach(item => {
        printStats(item)
      })
    } else {
      printStats(multiStats)
    }
    if (Object.prototype.toString.call(callback) === '[object Function]') {
      callback(multiStats)
    }
  }
}


module.exports = {
  generateLoaders,
  eslintLoader,
  genCssLoader,
  genLessLoader,
  genVueLoader,
  resolve,
  getPort,
  exec,
  genCompileCallback,
}
