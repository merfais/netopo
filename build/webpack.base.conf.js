const webpack = require('webpack')
const chalk = require('chalk')
const ProgressBarPlugin = require('progress-bar-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const utils = require('./utils.js')
const packageConfig = require('../package.json')

const base = {
  entry: {
    app: './demo/main.js'
  },
  output: {
    path: utils.resolve('./pages'),
    filename: '[name].[hash:6].js',
    chunkFilename: '[id].[chunkhash:6].js',
    publicPath: '',
  },
  resolve: {
    extensions: ['.js', '.vue', '.css', '.json'],
    alias: {
      demo: utils.resolve('demo'),
      src: utils.resolve('src'),
      dist: utils.resolve('dist'),
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [
          utils.resolve('src'),
          utils.resolve('demo')
        ]
      },
      {
        test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
        }
      },
      {
        test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader',
        options: {
          limit: 10000,
        }
      }
    ],
  },
  plugins: [
    new ProgressBarPlugin({
      format: '  build => :msg [:bar]  ' + chalk.green.bold(':percent')
        + chalk.yellow.bold('  ( :elapsed s )'),
      clear: false,
      incomplete: '-',
      width: 30,
    }),
    // https://github.com/ampedandwired/html-webpack-plugin
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'demo/index.html',
      inject: true
    }),
  ],
  externals: {
    Vue: 'Vue',
    'vue-router': 'VueRouter',
  },
  node: {
    // prevent webpack from injecting useless setImmediate polyfill because Vue
    // source contains it (although only uses it if it's native).
    setImmediate: false,
    // prevent webpack from injecting mocks to Node native modules
    // that does not make sense for the client
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty'
  },
}

const prod = {
  output: {
    publicPath: 'dist',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': '"production"',
      'process.env.VERSION': `'${packageConfig.version}'`
    }),
    // keep module.id stable when vendor modules does not change
    new webpack.HashedModuleIdsPlugin(),
    // enable scope hoisting
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
  ]
}

const dev = {
  profile: true,
  watch: true,
  watchOptions: {
    ignored: /node_modules/,
    poll: 1000,
  },
  module: {
    rules: [
      utils.eslintLoader,
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': '"development"'
    }),
    new webpack.HotModuleReplacementPlugin({
      requestTimeout: 1000,
    }),
    new webpack.NamedModulesPlugin(), // HMR shows correct file names in console on update.
    new webpack.NoEmitOnErrorsPlugin(),
  ],
  node: {
    console: true,
    global: true,
    process: true,
    Buffer: true,
    __filename: "mock",
    __dirname: "mock",
    setImmediate: true
  },
}

if (process.env.npm_config_analysis) {
  const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
  base.plugins.push(
    new BundleAnalyzerPlugin({
      analyzerHost: '0.0.0.0',
    })
  )
}

module.exports = {
  base,
  prod,
  dev,
}
