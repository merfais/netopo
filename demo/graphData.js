export function genNetworkNode(n = 10) {
  const data = []
  const shape = ['text', 'image']
  while(n--) {
    const item = {
      id: 'node' + n,
      shape: {
        type: 'image',
        href: require('./assets/vm.png')
      },
      label: {
        text: `vm-${n}`,
      },
      value: n
    }
    data.push(item)
  }
  return Object.freeze(data)
}

export function genNetworkEdge(e = 20, n = 10, multi = false) {
  const map = new Map()
  let m = n
  while(m--) {
    map.set(m, m)
  }
  const data = []
  let count =_.round(_.random(n / 40,  n / 5))
  while (count--) {
    const source = _.random(n - 1)
    map.delete(source)
    let rc = _.random(n / 20, n / 10)
    while(--rc > 0) {
      const target = _.random(n - 1)
      map.delete(target)
      const edge = {
        id: 'edge' + source + '_' + target,
        source: 'node' + source,
        target: 'node' + target,
        value: e,
      }
      if (multi) {
        const tem = [0, 1, 2, 3, 4, 5]
        const name = ['green', 'dashGreen', 'yellow', 'dashYellow', 'red', 'dashRed']
        const lines = _.shuffle(tem).slice(0, _.random(3) + 1).sort().map(i => name[i])
        edge.shape = {
          lines,
        }
      }
      data.push(edge)
    }
  }
  const rs = [...map.values()]
  let l = rs.length - 1
  while (l > 0) {
    const source = rs[l]
    let target = rs[l - 1] ? rs[l - 1] : 1
    const edge = {
      id: 'edge' + source + '_' + target,
      source: 'node' + source,
      target: 'node' + target,
      value: e,
    }
    if (multi) {
      const tem = [0, 1, 2, 3, 4, 5]
      const name = ['green', 'dashGreen', 'yellow', 'dashYellow', 'red', 'dashRed']
      const lines = _.shuffle(tem).slice(0, _.random(3) + 1).sort()
      edge.shape = {
        lines,
      }
    }
    data.push(edge)
    l -= 2
  }
  return Object.freeze(data)
}

export function genMultiGraph() {
  return Object.freeze({
    nodes: genNetworkNode(120),
    edges: genNetworkEdge(300, 120, true),
  })
}

export function genEdges(e = 20, n = 10) {
  const data = []
  while(e--) {
    const edge = {
      id: 'edge' + e,
      source: 'node' + _.random(1, n - 1),
      target: 'node' + _.random(1, n - 1),
      value: e,
    }
    data.push(edge)
  }
  return data
}

export function genNode (...args) {
  const id = args.join('_')
  return {
    id: 'node' + id,
    shape: {
      type: 'image',
      href: require('./assets/vm.png')
    },
    label: {
      text: 'vm' + id
    },
    value: _.random(50)
  }
}

export function genEdge(source, target) {
  return {
    id: 'edge_' + source + '_' + target,
    source,
    target,
    value: _.random(30),
  }
}

export function genSimpleGraph(n = 6) {
  const nodes = []
  const edges = []
  const links = [5, 8, 10, 5, 4, 10]
  while (n) {
    const src = genNode(n)
    nodes.push(src)
    let m = links[n - 1]
    while (m) {
      const dst = genNode(n, m)
      nodes.push(dst)
      const edge = genEdge(src.id, dst.id)
      edges.push(edge)
      m -= 1
    }
    n -= 1
  }
  edges.push(genEdge('node2_7', 'node5_1'))
  edges.push(genEdge('node6', 'node1_3'))
  edges.push(genEdge('node3', 'node4'))
  return Object.freeze({ nodes, edges })
}

export function genRandomGraph(n = 100, e = 100) {
  const edges = genEdges(e, n)
  const nodes = []
  while (n > 0) {
    nodes.push(genNode(n))
    n -= 1
  }
  console.log(nodes,edges)
  return { nodes, edges }
}

export function genLargeNodeGraph(edgeC = 100, nodeC = 100, nodeC2 = 10, circleC = 5) {
  const edges = []
  let nodes = {}
  while(edgeC--) {
    const edge = {
      id: 'edge' + edgeC,
      source: 'node' + _.random(1, nodeC - 1),
      target: 'node' + _.random(1, nodeC - 1),
      value: edgeC,
    }
    nodes[edge.source] = edge.source
    nodes[edge.target] = edge.target
    edges.push(edge)
  }
  let c = _.random(circleC) + 1
  while (c--) {
    const source = edges[_.random(edges.length - 1)].source
    let mm = nodeC2
    while(mm--) {
      const edge = {
        id: 'edgec' + mm + c,
        source,
        target: 'nodec' + c + _.random(1, mm - 1),
        value: mm,
      }
      nodes[edge.target] = edge.target
      edges.push(edge)
    }
  }
  nodes = _.map(nodes, id => {
    return {
      id,
      shape: {
        type: 'image',
        href: require('./assets/vm.png')
      },
      label: {
        text: id,
      },
    }
  })
  return { nodes, edges }
}

export function genDandelionGraph(n = 10, m = 2) {
  const nodes = []
  const edges = []
  while (m--) {
    nodes.push({
      id: 'node_s' + m,
      shape: {
        type: 'image',
        href: require('./assets/vm.png')
      },
      label: {
        text: 'node_s' + m,
      },
    })
    let i = 0
    while (i < n) {
      nodes.push({
        id: 'node_d' + m + i,
        shape: {
          type: 'image',
          href: require('./assets/vm.png')
        },
        label: {
          text: 'node_d' + m + i,
        },
      })
      edges.push({
        id: 'edge' + m + i,
        source: 'node_s' + m,
        target: 'node_d' + m + i,
      })
      i += 1
    }
  }
  return { nodes, edges }
}
